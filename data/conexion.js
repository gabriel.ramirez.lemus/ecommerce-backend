let mysql = require("mysql")
let pwh= require('password-hash')

let conexion = mysql.createConnection({
    host:'localhost',
    database:'bd_test',
    user:'root',
    password:'2021'
})


const conectar=()=>{
    conexion.connect((error)=>{
        if(error){
            throw error
        }else{
            console.log("Conexion exitosa!");
        }
    })
}




const consultaUsuario=(user,pass)=>{
        //conectar()
        return new Promise((res,rej)=>{
            conexion.query(`select * from usuario where nickname='${user}'`,(error,results,fields)=>{
                if(error){
                    rej(error)
                }else{
                    res(results[0])
                }
            })
        })
    
}

const login=(user,pass)=>{
    
    return new Promise((res,rej)=>{
        let passVerify=false
        const result={
            mensaje:'',
            status:0,
            usuario:{
                primer_nombre:"",
                segundo_nombre:"",
                apellido_paterno:"",
                apellido_materno:"",
                usuario:"",
                rol:""

            }
        }
        conexion.query(`select *  from usuario where nickname='${user}'`,(error,results,fields)=>{
            if(error){
                rej(error)
            }else{
                if(results.length==0){
                    res({...result,mensaje:'no existe el usuario',status:0})
                }else{
                    passVerify=pwh.verify(pass,results[0].password)
                    console.log("passVerify "+passVerify);
                    console.log(results[0]);
                    if(passVerify){

                        res({...result,mensaje:'usuario logueado',usuario:{
                            primer_nombre:results[0].primer_nombre,
                            segundo_nombre:results[0].segundo_nombre,
                            apellido_paterno:results[0].apellido_paterno,
                            apellido_materno:results[0].apellido_materno,
                            usuario:results[0].nickname,
                            rol:results[0].rol,
                            correo:results[0].correo_electronico,
                        },status:2})
                    }else{
                        res({...result,mensaje:'password incorrecta',status:1})
                    }
                }
            }
        })
    })
}





module.exports={
    consultaUsuario,
    login,
    conexion
}
