const con=require('./../conexion')
let pwh= require('password-hash')

let conexion = con.conexion

const insertarUsuario=(usuario)=>{

    let {primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,username,pass,fec_nac,genero,rol,correo}=usuario
    pass=pwh.generate(pass)
    return new Promise((res,rej)=>{
        conexion.beginTransaction(`INSERT INTO usuario (primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,nickname,password,fecha_nacimiento,sexo,rol,correo_electronico) VALUES ('${primer_nombre}','${segundo_nombre}','${apellido_paterno}','${apellido_materno}','${username}','${pass}','${fec_nac}','${genero}','${rol}','${correo}');`,(error,results,fields)=>{
            if(error){
                return  conexion.rollback(()=>{
                    rej(error)
                    
                })
            }
            conexion.commit((err)=>{
                if(err){
                    return conexion.rollback(()=>{
                        rej(err)
                    })
                }
                res("Usuario ingresado");
    
            })
        })
    })
    
}


module.exports={
    insertarUsuario
}